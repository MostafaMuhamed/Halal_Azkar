package Halal.halal.Halal.Food;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import Halal.halal.Halal.Model.UserModel;
import com.example.Halal.R;

import java.util.ArrayList;

public class FoodFragment extends Fragment
{
    private View view;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<UserModel> userModels;
    private getAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.food_fragment,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initialViews();
    }




    private void initialViews()
    {

        recyclerView = view.findViewById(R.id.recyclerFood);
        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        userModels = new ArrayList<>();
        userModels.add(new UserModel(" بِسْمِ اللهِ." +
                "فإنْ نسي في أَوَّلِهِ، فَليَقُلْ:" +
                "بِسْمِ اللَّه أَوَّلَهُ وَآخِرَهُ","الذكر عند الطعام والشراب"));
        userModels.add(new UserModel(" اَللَّهُمَّ بَارِكْ لَنَا فِيهِ, وَزِدْنَا مِنْهُ","الذكر عند شرب اللبن"));
        userModels.add(new UserModel(" الْحَمْدُ للهِ الَّذِي أَطْعَمَنِي هَذَا, وَرَزَقَنِيهِ مِنْ غَيْرِ حَوْلٍ مِّنِّي وَلاَ قُوَّةٍ." +
                "الْحَمْدُ لِلَّهِ كَثِيرًا طَيِّبًا مُبَارَكًا فِيهِ غَيْرَ مَكْفِيٍّ وَلَا مُوَدَّعٍ وَلَا مُسْتَغْنًى عَنْهُ رَبَّنَا","الذكر عند الفراغ من الطعام والشراب"));
        userModels.add(new UserModel(" أَفْطَرَ عِنْدَكُمُ الصَّائِمُونَ ، وَأَكَلَ طَعَامَكُمُ الأَبْرَارُ ، وَصَلَّتْ عَلَيْكُمُ الْمَلائِكَةُ","أذكار الضيف"));
        userModels.add(new UserModel(" كَانَ صَلَّى اللهُ عَلَيْهِ وَسَلَّمَ يَشْرَبُ فِي ثَلاَثَةِ أَنْفَاسٍ، إِذَا أَدْنَى الإِنَاءَ إِلَى فَمِهِ سَمَّى اللهَ تَعَالَى, وَإِذَا أَخَّرَهُ حَمِدَ اللهَ تَعَالَى، يَفْعَلُ ذَلِكَ ثَلاَثَ مَرَّاتٍ","هدى النبى فى الشرب"));


        adapter = new getAdapter(userModels);
        recyclerView.setAdapter(adapter);


    }

    private class getAdapter extends RecyclerView.Adapter<getAdapter.getVH>
    {
        ArrayList<UserModel> userModels;

        public getAdapter(ArrayList<UserModel> userModels) {
            this.userModels = userModels;
        }

        @NonNull
        @Override
        public getVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.items, parent, false);
            return new getVH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull getVH holder, int position) {
            UserModel model = userModels.get(position);
            holder.txtZekr.setText(model.getTextZekr());
            holder.txtNumber.setText(model.getTextNumber());

        }

        @Override
        public int getItemCount() {
            return userModels.size();
        }

        private class getVH extends RecyclerView.ViewHolder {

            private TextView txtZekr;
            private TextView txtNumber;

            private getVH(@NonNull View itemView)
            {
                super(itemView);
                txtZekr = itemView.findViewById(R.id.txt_items);
                txtNumber = itemView.findViewById(R.id.text_items);

            }
        }
    }
}
