package Halal.halal.Halal.Prayer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import Halal.halal.Halal.Model.UserModel;
import com.example.Halal.R;

import java.util.ArrayList;

public class PrayerFragment extends Fragment
{
    private View view;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<UserModel> userModels;
    private getAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.prayer_fragment,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initialViews();
    }




    private void initialViews()
    {

        recyclerView = view.findViewById(R.id.recyclerPrayer);
        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        userModels = new ArrayList<>();
        userModels.add(new UserModel(" \"رَبَّنَا ظَلَمْنَا أَنفُسَنَا وَإِن لَّمْ تَغْفِرْ لَنَا وَتَرْحَمْنَا لَنَكُونَنَّ مِنَ الْخَاسِرِينَ\"","آدم علية السلام"));
        userModels.add(new UserModel(" \"رَّبِّ اغْفِرْ لِي وَلِوَالِدَيَّ وَلِمَن دَخَلَ بَيْتِيَ مُؤْمِنًا وَلِلْمُؤْمِنِينَ وَالْمُؤْمِنَاتِ وَلَا تَزِدِ الظَّالِمِينَ إِلَّا تَبَارًا\" , \" رَبِّ إِنِّي أَعُوذُ بِكَ أَنْ أَسْأَلَكَ مَا لَيْسَ لِي بِهِ عِلْمٌ وَإِلاَّ تَغْفِرْ لِي وَتَرْحَمْنِي أَكُن مِّنَ الْخَاسِرِينَ\" , \" رَّبِّ أَنزِلْنِي مُنزَلاً مُّبَارَكاً وَأَنتَ خَيْرُ الْمُنزِلِينَ\"","نوح علية السلام"));
        userModels.add(new UserModel(" \"رَبَّنَا تَقَبَّلْ مِنَّا إِنَّكَ أَنْتَ السَّمِيعُ الْعَلِيمُ (127) رَبَّنَا وَاجْعَلْنَا مُسْلِمَيْنِ لَكَ وَمِنْ ذُرِّيَّتِنَا أُمَّةً مُسْلِمَةً لَكَ وَأَرِنَا مَنَاسِكَنَا وَتُبْ عَلَيْنَا إِنَّكَ أَنْتَ التَّوَّابُ الرَّحِيمُ (128)\" , \" رَبِّ اجْعَلْنِي مُقِيمَ الصَّلَاةِ وَمِن ذُرِّيَّتِي ۚ رَبَّنَا وَتَقَبَّلْ دُعَاءِ (40) رَبَّنَا اغْفِرْ لِي وَلِوَالِدَيَّ وَلِلْمُؤْمِنِينَ يَوْمَ يَقُومُ الْحِسَابُ (41)\" , \" رَبِّ هَبْ لِي حُكْمًا وَأَلْحِقْنِي بِالصَّالِحِينَ (83) وَاجْعَل لِّي لِسَانَ صِدْقٍ فِي الْآخِرِينَ (84) وَاجْعَلْنِي مِن وَرَثَةِ جَنَّةِ النَّعِيمِ (85)\" , \" رَّبَّنَا عَلَيْكَ تَوَكَّلْنَا وَإِلَيْكَ أَنَبْنَا وَإِلَيْكَ الْمَصِيرُ (4) رَبَّنَا لَا تَجْعَلْنَا فِتْنَةً لِّلَّذِينَ كَفَرُوا وَاغْفِرْ لَنَا رَبَّنَا ۖ  إِنَّكَ أَنتَ الْعَزِيزُ الْحَكِيمُ (5)\" , \" رَبِّ هَبْ لِي مِنَ الصَّالِحِينَ\"","إبراهيم علية السلام"));
        userModels.add(new UserModel(" \"إِنِّي تَوَكَّلْتُ عَلَى اللَّهِ رَبِّي وَرَبِّكُم ۚ مَّا مِن دَابَّةٍ إِلَّا هُوَ آخِذٌ بِنَاصِيَتِهَا ۚ إِنَّ رَبِّي عَلَىٰ صِرَاطٍ مُّسْتَقِيمٍ\"","هود علية السلام"));
        userModels.add(new UserModel(" \"رَبِّ انْصُرْنِي عَلَى الْقَوْمِ الْمُفْسِدِينَ\" , \" رَبِّ نَجِّنِي وَأَهْلِي مِمَّا يَعْمَلُونَ\"","لوط علية السلام"));
        userModels.add(new UserModel(" \"فَاطِرَ السَّمَاوَاتِ وَالْأَرْضِ أَنتَ وَلِيِّي فِي الدُّنْيَا وَالْآخِرَةِ ۖ تَوَفَّنِي مُسْلِمًا وَأَلْحِقْنِي بِالصَّالِحِينَ\"","يوسف علية السلام"));
        userModels.add(new UserModel(" \"وَسِعَ رَبُّنَا كُلَّ شَيْءٍ عِلْماً عَلَى اللّهِ تَوَكَّلْنَا رَبَّنَا افْتَحْ بَيْنَنَا وَبَيْنَ قَوْمِنَا بِالْحَقِّ وَأَنتَ خَيْرُ الْفَاتِحِينَ\"","شعيب علية السلام"));
        userModels.add(new UserModel(" \"رَبِّ إِنِّي ظَلَمْتُ نَفْسِي فَاغْفِرْ لِي\" , \" رَبِّ بِمَا أَنْعَمْتَ عَلَيَّ فَلَنْ أَكُونَ ظَهِيراً لِّلْمُجْرِمِينَ\" , \" رَبِّ إِنِّي لِمَا أَنزَلْتَ إِلَيَّ مِنْ خَيْرٍ فَقِيرٌ\" ,  \"رَبِّ اشْرَحْ لِي صَدْرِي (25) وَيَسِّرْ لِي أَمْرِي (26) وَاحْلُلْ عُقْدَةً مِّن لِّسَانِي (27) يَفْقَهُوا قَوْلِي (28)\"","موسى علية السلام"));
        userModels.add(new UserModel(" \"أَنِّي مَسَّنِيَ الضُّرُّ وَأَنتَ أَرْحَمُ الرَّاحِمِينَ\"","أيوب علية السلام"));
        userModels.add(new UserModel(" \"رَبِّ أَوْزِعْنِي أَنْ أَشْكُرَ نِعْمَتَكَ الَّتِي أَنْعَمْتَ عَلَيَّ وَعَلَى وَالِدَيَّ وَأَنْ أَعْمَلَ صَالِحًا تَرْضَاهُ وَأَدْخِلْنِي بِرَحْمَتِكَ فِي عِبَادِكَ الصَّالِحِينَ\"","سليمان علية السلام"));
        userModels.add(new UserModel(" \"لَّا إِلَهَ إِلَّا أَنتَ سُبْحَانَكَ إِنِّي كُنتُ مِنَ الظَّالِمِينَ\"","يونس علية السلام"));
        userModels.add(new UserModel(" \"رَبِّ هَبْ لِي مِن لَّدُنْكَ ذُرِّيَّةً طَيِّبَةً إِنَّكَ سَمِيعُ الدُّعَاء\" , \" رَبِّ لَا تَذَرْنِي فَرْداً وَأَنتَ خَيْرُ الْوَارِثِينَ\"","زكريا علية السلام"));
        userModels.add(new UserModel(" \"إِنَّمَا أَشْكُو بَثِّي وَحُزْنِي إِلَى اللَّهِ\"","يعقوب علية السلام"));

        adapter = new getAdapter(userModels);
        recyclerView.setAdapter(adapter);


    }

    private class getAdapter extends RecyclerView.Adapter<getAdapter.getVH>
    {
        ArrayList<UserModel> userModels;

        public getAdapter(ArrayList<UserModel> userModels) {
            this.userModels = userModels;
        }

        @NonNull
        @Override
        public getAdapter.getVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.items, parent, false);
            return new getVH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull getVH holder, int position) {
            UserModel model = userModels.get(position);
            holder.txtZekr.setText(model.getTextZekr());
            holder.txtNumber.setText(model.getTextNumber());

        }

        @Override
        public int getItemCount() {
            return userModels.size();
        }

        private class getVH extends RecyclerView.ViewHolder {

            private TextView txtZekr;
            private TextView txtNumber;

            private getVH(@NonNull View itemView)
            {
                super(itemView);
                txtZekr = itemView.findViewById(R.id.txt_items);
                txtNumber = itemView.findViewById(R.id.text_items);

            }
        }
    }
}
