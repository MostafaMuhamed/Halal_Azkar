package Halal.halal.Halal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.Halal.R;

import Halal.halal.Halal.Food.MainFood;
import Halal.halal.Halal.Morning.MainMorning;
import Halal.halal.Halal.Night.MainNight;
import Halal.halal.Halal.Others.MainOthers;
import Halal.halal.Halal.Prayer.MainPrayers;
import Halal.halal.Halal.Umrah.MainUmrah;


public class MainActivity extends AppCompatActivity
{
    private Button buttonMor,buttonNig,buttonPra,buttonFoo,buttonOth,buttonUmr;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialViews();
        getClick();
    }


    private void initialViews()
    {
        buttonMor = findViewById(R.id.btn_morning);
        buttonNig = findViewById(R.id.btn_night);
        buttonPra = findViewById(R.id.btn_prayer);
        buttonFoo = findViewById(R.id.btn_food);
        buttonOth = findViewById(R.id.btn_other);
        buttonUmr = findViewById(R.id.btn_umrah);
    }


    private void getClick()
    {
        buttonMor.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainMorning.class));
            }
        });


        buttonNig.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainNight.class));
            }
        });


        buttonOth.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainOthers.class));
            }
        });


        buttonPra.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainPrayers.class));
            }
        });


        buttonUmr.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainUmrah.class));
            }
        });


        buttonFoo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(getApplicationContext(), MainFood.class));
            }
        });
    }
}
