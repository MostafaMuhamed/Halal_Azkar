package Halal.halal.Halal.Model;

public class UserModel
{
    private String textZekr;
    private String textNumber;

    public UserModel()
    {

    }

    public UserModel(String textZekr, String textNumber)
    {
        this.textZekr = textZekr;
        this.textNumber = textNumber;
    }

    public String getTextZekr() {
        return textZekr;
    }

    public void setTextZekr(String textZekr) {
        this.textZekr = textZekr;
    }

    public String getTextNumber() {
        return textNumber;
    }

    public void setTextNumber(String textNumber) {
        this.textNumber = textNumber;
    }
}
