package Halal.halal.Halal.Others;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import Halal.halal.Halal.Model.UserModel;
import com.example.Halal.R;

import java.util.ArrayList;

public class OthersFragment extends Fragment
{
    private View view;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<UserModel> userModels;
    private getAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.other_fragment,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initialViews();
    }




    private void initialViews()
    {

        recyclerView = view.findViewById(R.id.recyclerOther);
        linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        userModels = new ArrayList<>();
        userModels.add(new UserModel(" الحمدُ للهِ الّذي كَساني هذا (الثّوب) وَرَزَقَنيه مِنْ غَـيـْرِ حَولٍ مِنّي وَلا قـوّة","دعاء لبس الثوب"));
        userModels.add(new UserModel(" اللهم لك الحمد أنت كسوتنيه، أسألك من خيره ","دعاء لبس الثوب الجديد"));
        userModels.add(new UserModel(" لا إله إلا الله العظيم الحليم، لا إله إلا الله رب العرش العظيم، لا إله إلا الله رب السماوات، ورب الأرض ورب العرش الكريم" +
                "اللهم رحمتك أرجو فلا تكلني إلى نفسي طرفة عين وأصلح لي شأني كله ، لا إله إلا أنت" +
                "لا إله إلا أنت سبحانك إني كنت من الظالمين" +
                "الله الله رب لا أشرك به شيئا","دعاء الكرب"));
        userModels.add(new UserModel(" اللهم إني عبدك ابن عبدك ابن أمتك ناصيتي بيدك ماض في حكمك ، عدل في قضاؤك أسألك بكل اسم هو لك سميت به نفسك أو أنزلته في كتابك ، أو علمته أحداً من خلقك أو استأثرت به في علم الغيب عندك أن تجعل القرآن ربيع قلبي ، ونور صدري وجلاء حزني وذهاب همي" +
                " اللهم إني أعوذ بك من الهم والحزن والعجز والكسل والبخل والجبن ، وضلع الدين وغلبة الرجال","دعاء الهم والحزن"));
        userModels.add(new UserModel(" اللهم اكفنى بحلالك عن حرامك وأغننى بفضلك عمن سواك","دعاء قضاء الدين"));
        userModels.add(new UserModel(" اللهم إنى أسألك خيرها، وخير ما فيها، وخير ما أرسلت به، وأعوذ بك من شرها، وشر ما فيها وشر ما أرسلت به","دعاء الريح"));
        userModels.add(new UserModel(" سبحان الله الذي يسبح الرعد بحمده والملائكة من خيفته","دعاء الرعـد"));
        userModels.add(new UserModel(" السلام عليكم أهل الديار من المؤمنين والمسلمين، وإنا إن شاء الله بكم لاحقون ويرحم الله المستقدمين منا والمستأخرين، أسأل الله لنا ولكم العافية","دعاء زيارة القبور"));
        userModels.add(new UserModel(" اللهم صيباَ نافعاَ","دعاء نزول المطر"));
        userModels.add(new UserModel(" بسم الله ، الحمد لله ، سبحان الذي سخر لنا هذا وماكنا له مقرنين وإنا إلى ربنا لمنقلبون، الحمد لله ، الحمد لله ، الحمد لله ، الله اكبر ، الله أكبر ، الله أكبر ، سبحانك اللهم إني ظلمت نفسي فاغفر لي ، فإنه لايغفر الذنوب إلا أنت","دعاء ركوب الدابة أو ما يقوم مقامها ( السيارة أو القطار أو الطائرة أو السفينة)"));
        userModels.add(new UserModel(" اللهم إنا نسألك في سفرنا هذا البر والتقوى، ومن العمل ماترضى ، اللهم هون علينا سفرنا هـذا واطوِ عنا بعده، اللهم أنت الصاحب في السفر، والخليفة في الأهل، اللهم إني أعوذ بك من وعثاء السفر وكآبة المنظر وسوء المنقلب في المال والأهل","دعاء السفر"));
        userModels.add(new UserModel(" استودعكم الله الذي لا تضيع ودائعه","دعاء المسافر للمقيم"));
        userModels.add(new UserModel(" أستودع ُ الله دينك وأمانتك ، وخواتيم عملك زودك الله التقوى، وغفر ذنبك، ويسر لك الخير حيث ما كنت","دعاء المقيم للمسافر"));
        userModels.add(new UserModel(" أعوذ بكلمات الله التامات من شر ما خلق","دعـــاء النـــزول في مكــان"));
        userModels.add(new UserModel(" رب اغفر لي رب اغفر لي اللهم اغفر لي ، وارحمني واهدني واجبرني وعافني وارزقني وارفعني","الدعاء بين السجدتين"));
        userModels.add(new UserModel(" اللهم لاسهل إلا ماجعلته سهلا وأنت تجعل الحزن إذا شئت سهلا","دعـاء من استصعب عليه أمر"));
        userModels.add(new UserModel(" اللهم إني أ عوذ بك أن أشرك بك وأنا أعلم ، وأستغفرك لما لا أعلم","دعـاء الخوف من الشــرك"));
        userModels.add(new UserModel(" رب اغفر لي وتب عليّ إنك أنت التواب الغفور","ما يقال في المجلس"));
        userModels.add(new UserModel(" سبحانك اللهم وبحمدك ، أشهد أن لا إله إلا أنت أستغفرك وأتوب إليك","كفارة المجلس"));
        userModels.add(new UserModel(" الحمد لله الذي عافاني مما ابتلا به وفضلني على كثير ممن خلق تفضيلاً","دعاء من رأى مبتلى"));
        userModels.add(new UserModel(" أعوذ بالله من الشيطان الرجيـم","دعـاء الغـضـب"));
        userModels.add(new UserModel( " بارك الله لك ، وبارك عليك ، وجمع بينكما في خير","الدعاء للمتزوج"));
        userModels.add(new UserModel(" اللهم إني أسألك خيرها وخير ماجبلتها عليه وأعوذ بك من شرها وشر ماجبلتها عليه ، وإذا اشترى بعيراً فليأخذ بذروة سنامه وليقل مثل ذلك","دعاء المتزوج لنفسه"));
        userModels.add(new UserModel(" بسم الله ـ اللهم جنبنا الشيطان، وجنب الشيطان مارزقتنا","الدعاء قبل الجماع"));
        userModels.add(new UserModel(" إذا عطـس أحـدكم فليقل : الحمـد لله ، وليقل له أخـوه ، أو صاحبه : يرحمـك الله فـإذا قال له : يرحمك الله ، فليقل : يهديكم الله ويصلح بالكم","دعاء العطاس"));
        userModels.add(new UserModel("ذهب الضمـأ ، وأبتلت العروق ، وثبت الأجر إن شاء الله","الدعاء عند افطار الصائم"));
        userModels.add(new UserModel(" أفطر عندكم الصائمون ، وأكل طعامكم الأبرار ، وصلت عليكم الملائكة","الدعاء إذا أفطر عند أهل بيت"));
        userModels.add(new UserModel(" الله أكبر ، اللهم أهله علينا بالأمن ، والإيمان والسلامة ، والإسلام ، والتوفيق لما تحب وترضى ربنا وربك الله","دعاء رؤية الهلال"));
        userModels.add(new UserModel(" إنا لله وإنا إليه راجعون اللهم أجرني في مصيبتي واخلف لي خيرا منها","دعاء من أصيب بمصيبة"));
        userModels.add(new UserModel(" لابأس طهور إن شاء الله مامن عبد مسلم يعود مريضاً لم يحضر أجله فيقول سبع مرات : أسأل الله العظيم رب العرش العظيم أن يشفيك إلا عوفي","الدعاء للمريض في عيادته"));
        userModels.add(new UserModel(" اللهم اغفر لي وارحمني والحقني بالرفيق الأعلى","دعاء المريض الذي يئس من حياته"));
        userModels.add(new UserModel(" أعيذكما بكلمات الله التامة من كل شيطان وهامة ، ومن كل عين لامة","ما يعوذ به الأولاد"));
        userModels.add(new UserModel("حسـبنا الله ونعـم الـوكـيل" +
                "اللهم إنا نجعلك في نحورهم ونعوذ بك من شرورهم" +
                " اللهم أنت عضدي ، وأنت نصيري ، بك أجول وبك أصول وبك أقاتل","دعاء لقاء العدو وذي السلطان"));
        userModels.add(new UserModel(" قال جابر بن عبدالله رضي الله عنهما : كان رسول الله صلى الله عليه وسلم يعلمنا الاستخارة في الامور كلها كما يعلمنا السورة من القرآن ، يقول : ( إذا هم أحدكم بالأمر فليركع ركعتين من غير الفريضة ثم ليقل : اللهم إني أستخيرك بعلمك ، وأستقدرك بقدرتك وأسألك من فضلك العظيم فإنك تقدر ولا أقدر وتعلم ولاأعلم وأنت علام الغيوب ، اللهم إن كنت تعلم أن هذا الأمر ـ ويسمي حاجته ـ خيرا لي في ديني ومعاشي وعاقبة أمري ـ أو قال عاجله وآجله ـ فأقدره لي ويسره لي ثم بارك لي فيه ، وإن كنت تعلم أن هذا الأمر شر لي في ديني ومعاشي وعاقبة أمري ـ أو قال عاجله وآجله ـ فاصرفه عني واصرفني عنه واقدر لي الخير حيث كان ثم ارضني به)","دعـاء صـلاة الاستخـارة"));
        userModels.add(new UserModel(" سجد وجهي للذي خلقه ،وشق سمعه وبصره بحوله وقوته ( فتبارك الله احسن الخالقين )","دعـاء سجود التلاوة"));
        userModels.add(new UserModel("اللهم باعد بيني وبين خطاياي كما باعدت بين المشرق والغرب ، اللهم نقني من خطاياي كما ينقى الثوب الأبيض من الدنس، اللهم اغسلني من خطاياي بالثلج والماء والبرد" +
                " سبحانك اللهم وبحمدك وتبارك اسمك وتعالى جدُّك ولاإله غيرك","دعـــــاء الاستفــتــــاح"));
        userModels.add(new UserModel("لا إله إلا الله وحده لا شريك له، له الملك وله الحمد يحيي ويميت وهو حي لا يموت بيده الخير وهو على كل شيء قدير (كتب الله له ألف ألف حسنة ومحا عنه ألف ألف سيئة ورفع له ألف الف درجة وفي رواية: وبنى له بيتا في الجنة)" +
                " بسم الله، اللهم إني أسألك خير هذه السوق، وخير ما فيها، وأعوذ بك من شرها وشر ما فيها، اللهم إني أعوذ بك أن أصيب بها يميناً فاجرةً، أو صفقة خاسرة","دعاء دخول السوق"));
        userModels.add(new UserModel(" كما أخبرنا رسول الله صلى الله عليه وسلم ((ما من عبد يذنب ذنباً فيتوضأ فيحسن الطهور،ثم يقوم فيصلي ركعتين ، ثم يستغفر الله لذلك الذنب إلاَّ غُفر له ))","عنــــد فعــــل الذنب أو ارتكاب المعصية"));
        userModels.add(new UserModel(" الدعاء عند صياح الديك : (( اللهم إني أسألك من فضلـــك )). ((إذا سمعتم صياح الديك[من الليل]،فاسألوا الله من فَضْلِهِ فإنها رأت ملكاً و إذا سمعتم نهيق الحمار،فتعوَّذوا بالله من الشيطان فإنها رأت شَيْطَاناً)) , الدعاء عند صياح الديك و نهيق الحمار و نباح الكلاب: (( أعوذ بالله من الشيطان الرجيم ))","لدعـــاء عنــــد سماع أصوات الحيوانات"));
        userModels.add(new UserModel(" اللهم أنت ربي لا إله إلا أنت ، خلقتني وأنا عبدك وأنا على عهدك ووعدك ما استطعت أعوذ بك من شر ما صنعت، أبوئ لك بنعمتك على وأبوئ بذنبي ، فاغفر لي فانه لا يغفر الذنوب إلا أنت","سيد الاستغفار"));
        userModels.add(new UserModel(" ضع يدك على الذي تألَّم من جسدك وقل: بسم الله ،ثلاثاً ،وقل سبع مرات: أعوذُ بالله وقُدْرَتِهِ من شَرِّ مَا أَجِدُ واُحَاذِرُ","الدعـــاء إذا أحسست بوجع في جسدك"));
        userModels.add(new UserModel(" مامن عبد يذنب ذنباً فيحسـن الطهور ثم يقوم فيصلي ركعتين ، ثم يستغــفــر الله إلا غفر له","مايقول ويفعل من أذنب ذنباً"));
        userModels.add(new UserModel("  بِسْـمِ اللهِ وَلَجْنـا، وَبِسْـمِ اللهِ خَـرَجْنـا، وَعَلـى رَبِّنـا تَوَكّلْـنا","أذكار الدخول إلى المنزل"));
        userModels.add(new UserModel("بِسْمِ اللهِِ ، تَوَكَّلْـتُ عَلى اللهِ وَلا حَوْلَ وَلا قُـوَّةَ إِلاّ بِالله" +
                "اللّهُـمَّ إِنِّـي أَعـوذُ بِكَ أَنْ أَضِـلَّ أَوْ أُضَـل ، أَوْ أَزِلَّ أَوْ أُزَل ، أَوْ أَظْلِـمَ أَوْ أَُظْلَـم ، أَوْ أَجْهَلَ أَوْ يُـجْهَلَ عَلَـيّ","أذكار الخروج من المنزل"));

        adapter = new getAdapter(userModels);
        recyclerView.setAdapter(adapter);


    }

    private class getAdapter extends RecyclerView.Adapter<getAdapter.getVH>
    {
        ArrayList<UserModel> userModels;

        public getAdapter(ArrayList<UserModel> userModels) {
            this.userModels = userModels;
        }

        @NonNull
        @Override
        public getAdapter.getVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.items, parent, false);
            return new getVH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull getVH holder, int position) {
            UserModel model = userModels.get(position);
            holder.txtZekr.setText(model.getTextZekr());
            holder.txtNumber.setText(model.getTextNumber());

        }

        @Override
        public int getItemCount() {
            return userModels.size();
        }

        private class getVH extends RecyclerView.ViewHolder {

            private TextView txtZekr;
            private TextView txtNumber;

            private getVH(@NonNull View itemView)
            {
                super(itemView);
                txtZekr = itemView.findViewById(R.id.txt_items);
                txtNumber = itemView.findViewById(R.id.text_items);

            }
        }
    }
}