package Halal.halal.Halal.Morning;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.Halal.R;

public class MorningSebha extends Fragment
{
    private View view;
    private TextView textView;
    private CardView imageSebha,imageReset;
    private int numberCounter = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.sebha,container,false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initialViews();
        getClicked();
    }

    private void initialViews()
    {
        textView = view.findViewById(R.id.txt_tasbeh);
        imageSebha = view.findViewById(R.id.img_sebha);
        imageReset = view.findViewById(R.id.img_refresh);
    }

    private void getClicked()
    {
        imageSebha.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                numberCounter = numberCounter + 1;
                textView.setText(numberCounter+"");
            }
        });

        imageReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                numberCounter = 0;
                textView.setText(numberCounter+"");
            }
        });
    }
}
